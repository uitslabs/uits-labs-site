FROM node:4.2
EXPOSE 3000

COPY data/local.js /usr/src/app/data/local.js
COPY lib /usr/src/app/lib
COPY locales /usr/src/app/locales
COPY node_modules /usr/src/app/node_modules
COPY public /usr/src/app/public
COPY scripts /usr/src/app/scripts
COPY views /usr/src/app/views
COPY app.js /usr/src/app/app.js
COPY package.json /usr/src/app/package.json
COPY express-static /usr/src/app/express-static

WORKDIR /usr/src/app
CMD node app.js