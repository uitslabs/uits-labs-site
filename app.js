/* jshint node:true */
var watchify = require('watchify');
var fs = require('fs');

var privatePath = "additional.json";
//if (process.argv.length > 2)
//  privatePath = process.argv[2];

var additional = {};
if (fs.existsSync(privatePath)) {
  additional = JSON.parse(fs.readFileSync(privatePath));
}

var site = require('apostrophe-site')({

  // This line is required and allows apostrophe-site to use require() and manage our NPM modules for us.
  root: module,
  shortName: 'uits-labs',
  hostName: 'uits-labs.ru',
  title: 'uits-labs',
  sessionSecret: additional.sessionSecret ||'EcRzXsWrt77ai2nmPg7fQG57PQpJFl',
  adminPassword: additional.adminPassword || 'demo',

  // Force a2 to prefix all of its URLs. It still
  // listens on its own port, but you can configure
  // your reverse proxy to send it traffic only
  // for URLs with this prefix. With this option
  // "/" becomes a 404, which is supposed to happen!

  // prefix: '/test',

  // If true, new tags can only be added by admins accessing
  // the tag editor modal via the admin bar. Sometimes useful
  // if your folksonomy has gotten completely out of hand
  lockTags: false,

  // Give users a chance to log in if they attempt to visit a page
  // which requires login
  secondChanceLogin: true,

  locals:  require('./lib/locals.js'),

  // you can define lockups for areas here
  // lockups: {},

  // Here we define what page templates we have and what they will be called in the Page Types menu.

  // For html templates, the 'name' property refers to the filename in ./views/pages, e.g. 'default'
  // refers to '/views/pages/default.html'.

  // The name property can also refer to a module, in the case of 'blog', 'map', 'events', etc.

  pages: {
    types: [
      { name: 'default', label: 'Default Page' },
      { name: 'home', label: 'Home Page' },
      { name: 'course', label: 'Страница курса' },
      { name: 'coursesContainer', label: 'Набор курсов' },
      { name: 'blog', label: 'Blog' }
    ]
  },

  lockups: {
    left: {
      label: 'Left',
      tooltip: 'Inset Left',
      icon: 'icon-arrow-left',
      widgets: [ 'slideshow', 'video' ],
      slideshow: {
        size: 'one-half'
      }
    },
    right: {
      label: 'Right',
      tooltip: 'Inset Right',
      icon: 'icon-arrow-right',
      widgets: [ 'slideshow', 'video' ],
      slideshow: {
        size: 'one-half'
      }
    }
  },

  // These are the modules we want to bring into the project.
  modules: {
    // Styles required by the new editor, must go FIRST
    'apostrophe-editor-2': {},
    'apostrophe-ui-2': {},
    'apostrophe-blog-2': {
      perPage: 5,
      pieces: {
        addFields: [
          {
            name: '_author',
            type: 'joinByOne',
            withType: 'person',
            idField: 'authorId',
            label: 'Author'
          }
        ]
      }
    },
    'apostrophe-people': {
      addFields: [
        {
          name: '_blogPosts',
          type: 'joinByOneReverse',
          withType: 'blogPost',
          idField: 'authorId',
          label: 'Author',
          withJoins: [ '_editor' ]
        },
        {
          name: 'thumbnail',
          type: 'singleton',
          widgetType: 'slideshow',
          label: 'Picture',
          options: {
            aspectRatio: [100,100]
          }
        }
      ]
    },
    'apostrophe-groups': {},
    'apostrophe-browserify': {
      files: ["./public/js/modules/_site.js"]
    },
    //'apostrophe-fancy-page': {
    //},
    'apostrophe-demo-login': {
    },
    'course': {
      extend: 'apostrophe-fancy-page'
    },
    'coursesContainer': {
      extend: 'apostrophe-fancy-page',
      name: 'coursesContainer',
      label: 'Набор курсов'
    }

  },

  // These are assets we want to push to the browser.
  // The scripts array contains the names of JS files in /public/js,
  // while stylesheets contains the names of LESS files in /public/css
  assets: {
    stylesheets: ['site'],
    scripts: ['_site-compiled']
  },

  configureNunjucks: function(env) {
    env.addFilter('keysSize', function(obj) {
      return Object.keys(obj).length;
    });
  },

  afterInit: function(callback) {

    var oldPutPage = site.apos.putPage;
    site.apos.putPage = function(req, slug, options, page, callback) {
      page.lastModified = new Date();
      oldPutPage(req, slug, options, page, callback)
    };

    //  console.log(oldPutPage);

    // We're going to do a special console log now that the
    // server has started. Are we in development or production?
    var locals = require('./data/local');
    if(locals.development || !locals.minify) {
      console.log('Apostrophe Sandbox is running in development.');
    } else {
      console.log('Apostrophe Sandbox is running in production.');
    }

    callback(null);
  }

});

// a frontend server to handle static content, is required because it overrides some apostrophe paths
var proxy = require('express-http-proxy');
var express = require('express');
var app = express();

app.use('/forstudents/aiml/jupyter/', express.static(__dirname + '/express-static/jupyter'));
app.use('/dosbox/', express.static(__dirname + '/express-static/dosbox'));
app.use('/', proxy('localhost:3000'));

app.listen(3020)
