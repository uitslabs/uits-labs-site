# Docker installation

Download required packages locally by running

    npm install
    
Build a `uits-labs-site` image by `docker-compose`:

    docker-compose build
    
Run the docker compose to get the site on the 3000-port

    docker-compose up
    
## Prod env

When deployng to prod server don't forget to configure a proper
local volume for database instead of `./mongo-db/db` in the `docker-compose.yaml`
    
# Apostrophe Sandbox

The Apostrophe Sandbox is a complete starting point for developing content-managed websites with Apostrophe. Please see the [Apostrophe documentation](http://apostrophenow.org) for a tutorial that begins with setting up this sandbox project.
