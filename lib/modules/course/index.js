var _ = require('lodash');

module.exports = course;

function course(options, callback) {
    return new course.Course(options, callback);
}

course.Course = function (options, callback) {
    var self = this;

    _.extend(options, {
        name: 'course',
        label: 'Учебный курс',
        addFields: [
            {
                name: 'school',
                label: 'Направление',
                type: 'select',
                choices: [{
                    value: 'Прикладная информатика',
                    label: 'Прикладная информатика'
                }, {
                    value: 'МТО',
                    label: 'МТО'
                }]
            },
            {
                name: 'semester',
                label: 'Семестр',
                type: 'integer'
            }
        ]
    });

    module.exports.Super.call(this, options, null);

    var beforePutPage = self.beforePutPage;

    self.beforePutPage = function(req, slug, options, page, callback)  {
        //snippet.publicationDate = self._apos.sanitizeDate(data.publicationDate, snippet.publicationDate);
        console.log("before beforePutPage:"/*, page*/);
        return beforePutPage(req, slug, options, page, callback) ;
    };

    var beforePutOne = self.beforePutOne;

    self.beforePutOne = function(req, slug, options, page, callback)  {
        //snippet.publicationDate = self._apos.sanitizeDate(data.publicationDate, snippet.publicationDate);
        console.log("before beforePutOne:"/*, page*/);
        return beforePutOne(req, slug, options, page, callback) ;
    };

    if (callback) {
        process.nextTick(function () { return callback(null); });
    }
};